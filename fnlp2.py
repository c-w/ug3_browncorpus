#!/usr/bin/python

import collections
import os
import random
import sys
import time

__NO_DISPLAY__ = 'DISPLAY' not in os.environ
__STDOUT_REDIRECTED__ = os.fstat(0) != os.fstat(1)
__RANDOM_SEED__ = 123456
__PYTHON_PATH__ = '/group/ltg/projects/lcontrib/lib/python2.6/site-packages/'

# make sure we always try to import NLTK from the provided directory
# (the version in /usr/lib on DICE will crash when evaluating the HMMs)
sys.path.insert(0, __PYTHON_PATH__)

import nltk
import nltk.corpus
import nltk.probability
import nltk.tag
import nltk.tokenize

BROWN_CORPUS = nltk.corpus.brown.tagged_words(categories='news')
BROWN_TAGGED_SENTS = nltk.corpus.brown.tagged_sents(categories='news')
BROWN_WORDS = [word for (word, _) in BROWN_CORPUS]
BROWN_TAGS = [tag for (_, tag) in BROWN_CORPUS]
BROWN_TAGSET = list(set(BROWN_TAGS))

def print_function_name(fn):
    """Decorator to avoid having to explictly print out the question number in
    each function"""
    def wrapped():
        print '#### %s ####' % fn.__name__.replace('_', ' ').title()
        rval = fn()
        print '\n\n'
        return rval
    return wrapped

def random_split(li, split_percentage):
    """Generates a random permutation of |li| and splits it into a list of size
    len(|li|) * |split_percentage| and the rest. Does not modify |li|."""
    num_split_elements = int(round(split_percentage * len(li)))
    random_permutation = li[:]
    random.shuffle(random_permutation)
    big_set = random_permutation[:num_split_elements]
    small_set = random_permutation[num_split_elements:]
    return (big_set, small_set)

def tag_to_pos(tag):
    """Make |tag| in the Brown Corpus Tagset more human-readable."""
    import string
    tag = tag.upper()
    if tag.startswith('NN'):
        return 'common noun'
    if tag.startswith('NP'):
        return 'proper noun'
    if tag.startswith('VB'):
        return 'verb'
    if tag.startswith('JJ'):
        return 'adjective'
    if tag in string.punctuation:
        return 'punctuation'
    if tag.startswith('PP'):
        return 'pronoun'
    if tag.startswith('RB'):
        return 'adverb'
    if tag.startswith('CC') or tag.startswith('CS'):
        return 'conjunction'
    if tag.startswith('CD'):
        return 'numeral'
    if tag.startswith('IN'):
        return 'preposition'
    if tag.startswith('AT'):
        return 'article'
    if tag.startswith('TO'):
        return 'infinitival to'
    return 'OTHER'

def table(*lists):
    """Formats lists into a single table-style string for pretty-printing."""
    def tablify(li, lsep='|', rsep='|', pad=' ', delim='-', has_header=True):
        li = [str(elem) for elem in li]
        lens = [len(elem) for elem in li]
        max_len = max(lens) + 2
        pretty_li = []
        for (i, elem) in enumerate(li):
            cur_len = lens[i]
            pad_len = max_len - cur_len
            lpad = pad * (pad_len / 2)
            rpad = pad * (pad_len / 2 if pad_len % 2 == 0 else pad_len / 2 + 1)
            pretty_li.append(lsep + lpad + elem + rpad + rsep)
        if delim is not None:
            delim = delim * (len(lsep) + max_len + len(rsep))
            pretty_li = [delim] + pretty_li + [delim]
            if has_header:
                pretty_li.insert(2, delim)
        return pretty_li
    def zipwith(fun, *lists):
        """Returns the resulf of applying function |fun| to the first elements
        of the lists in |lists|, to the second elements of the lists in |lists|,
        ..., to the nth elements of the lists in |lists|.
        >>> zipwith(lambda a, b: a + b, [1, 2, 3], [4, 5, 6])
        [5, 7, 9]
        >>> zipwith(lambda a, b: a + b, [1, 2, 3], [4, 5, 6], [7, 8, 9])
        [12, 15, 18]
        >>> zipwith(lambda a, b: a + b, ['a', 'b'], ['A', 'B'], ['1', '2'])
        ['aA1', 'bB2']
        >>> zipwith(lambda a, b: a * b, ['a', 'b', 'c'], [1, 2, 3])
        ['a', 'bb', 'ccc']
        """
        return [reduce(fun, zipped) for zipped in zip(*lists)]
    pretty_lists = [tablify(lists[0])]
    pretty_lists.extend([tablify(li, lsep='') for li in lists[1:]])
    return '\t' + '\n\t'.join(zipwith(lambda a, b: a + b, *pretty_lists))

def evaluate_tagger(tagger, sentences):
    """|sentences| is a list of (word, tag) pairs. Use |tagger| to tag the words
    and display a comparison of the predicted tags and the actual tags."""
    if not type(sentences) is list:
        sentences = [sentences]
    for sentence in sentences:
        (words, tags) = [list(li) for li in zip(*sentence)]
        predicted = [tag for (_, tag) in tagger.tag(words)]
        prediction_correct = ['x' if tags[i] == predicted[i] else ''
                              for i in range(len(tags))]
        print table(['Word'] + words, ['Predicted'] + predicted,
                    ['Correct?'] + prediction_correct, ['Actual'] + tags)

def timeit(fun, *args, **kwargs):
    """Run fun(args, kwargs), display the time it took, return the result."""
    if '_pre_message' in kwargs:
        pre_message = kwargs.pop('_pre_message')
        print pre_message
    else:
        pre_message = None
    if '_postpend_time' in kwargs:
        postpend_time = kwargs.pop('_postpend_time')
    else:
        postpend_time = False
    if '_return_time' in kwargs:
        return_time = kwargs.pop('_return_time')
    else:
        return_time = False
    print 'This may take a while...'
    start_time = time.time()
    rval = fun(*args, **kwargs)
    end_time = time.time()
    time_to_rval = end_time - start_time
    stdout_clearline()
    if postpend_time:
        if pre_message:
            stdout_clearline()
            print pre_message + ' -',
        print 'Done in %.4f seconds' % time_to_rval
    if return_time:
        return (rval, time_to_rval)
    return rval

def stdout_clearline():
    """Clears the last line on the standard output."""
    if not __STDOUT_REDIRECTED__:
        sys.stdout.write('\x1b[1A' + '\x1b[2K')

def bar_chart(labels, values, title='', xlabel='', ylabel='', width=0.35,
              ylim=(0.0, 1.0)):
    """Creats a bar chart with bars of heights |values| and names |labels|."""
    if __NO_DISPLAY__:
        return
    import pylab
    def label(axis, bars, labels, ha='center', va='bottom'):
        """Adds labels to a bar chart on some axis."""
        for (i, rect) in enumerate(bars):
            x = rect.get_x() + rect.get_width() / 2.0
            y = 1.05 * rect.get_height()
            label = labels[i]
            label = '%.4f' % label if isinstance(label, float) else str(label)
            axis.text(x, y, label, ha=ha, va=va)
    def values_to_colors(values):
        """Maps the numbers in values to a color between red (lowest value) and
        green (highest value) proportional to the number's relative size
        compared to the other values."""
        def scale(x, xmin, xmax, tmin, tmax):
            """Maps x in [xmin..xmax] to [tmin..tmax]."""
            return (x - xmin) / (xmax - xmin) * (tmax - tmin) + tmin
        _scale = lambda x: scale(x, min(values), max(values), 0., 1.)
        return [(1 - _scale(float(x)), _scale(float(x)), 0) for x in values]
    fig = pylab.figure()
    ax = fig.add_subplot(111)
    cols = values_to_colors(values)
    idx = range(len(labels))
    rects = ax.bar(idx, values, align='center', width=width, color=cols)
    ax.set_xticks(idx)
    ax.set_xticklabels(labels)
    ax.set_ylim(ylim)
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    label(ax, rects, values)
    pylab.show()

@print_function_name
def question_1():
    # build frequency distribution over tags in brown corpus
    tags_fdist = nltk.probability.FreqDist(BROWN_TAGS)
    # print tags in order of decreasing frequency
    tags = [elem[0] for elem in tags_fdist.items()]
    counts = [elem[1] for elem in tags_fdist.items()]
    print 'Tags found in the Brown Corpus (in order of decreasing frequency)'
    print table(['Tag'] + tags, ['Count'] + counts)
    # analyse parts of speech of 20 most common tags
    tag_d = collections.defaultdict(int)
    for (tag, count) in tags_fdist.items()[:20]:
        tag_d[tag_to_pos(tag)] += count
    top20_tags_counts = sorted(tag_d.items(), key=lambda t: t[1], reverse=True)
    top20_tags = [elem[0] for elem in top20_tags_counts]
    top20_counts = [elem[1] for elem in top20_tags_counts]
    top20_percentages = ['%.2f' % (100.0 * c / sum(top20_counts))
                         for c in top20_counts]
    print 'Top20 most common parts of speech in the Brown Corpus'
    print table(['Part of Speech'] + top20_tags, ['%'] + top20_percentages,
                ['Count'] + top20_counts)

@print_function_name
def question_2():
    # collect the different types of tags seen for every word
    word_tags_dict = collections.defaultdict(list)
    for (word, tag) in BROWN_CORPUS:
        word = word.lower()
        if tag not in word_tags_dict[word]:
            word_tags_dict[word].append(tag)
    # build a freqency distribution over word->tag types
    tagcount_fdist = nltk.probability.FreqDist()
    for word in word_tags_dict:
        num_tags = len(word_tags_dict[word])
        tagcount_fdist.inc(word, num_tags)
    # find and print word with greatest number of distinct types
    for (i, elem) in enumerate(tagcount_fdist.items()[:-1]):
        (cur_word, cur_count) = elem
        pos_dict = collections.defaultdict(list)
        for tag in word_tags_dict[cur_word]:
            pos_dict[tag_to_pos(tag)].append(tag)
        (next_word, next_count) = tagcount_fdist.items()[i + 1]
        print ('The word with the most tag-types is "%s" (%d types%s).'
                % (cur_word, cur_count,
                   ' - tied' if next_count == cur_count or i != 0 else ''))
        print ('These pos-types represent the following parts of speech:\n\t%s'
                % ('\n\t'.join(['%s (%s)' % (key, ', '.join(pos_dict[key]))
                                for key in pos_dict])))
        if next_count < cur_count:
            break

@print_function_name
def question_3():
    # build inverse tag bigrams and build a conditional frequency distribution
    inv_tag_bigrams = [(b, a) for (a, b) in nltk.bigrams(BROWN_TAGS)]
    tag_cfdist = nltk.probability.ConditionalFreqDist(inv_tag_bigrams)
    # accumulate the counts of all tags occuring before nouns
    tag_is_noun = lambda tag: tag.startswith('N')
    tags_before_noun_dict = collections.defaultdict(int)
    for tag in BROWN_TAGSET:
        if not tag_is_noun(tag):
            continue
        for (predecessor, count) in tag_cfdist[tag].items():
            tags_before_noun_dict[predecessor] += count
    # print the most common predecessors
    predecessors = sorted(tags_before_noun_dict, key=tags_before_noun_dict.get,
        reverse=True)[:5]
    predecessors_counts = [tags_before_noun_dict[elem] for elem in predecessors]
    predecessors_pos = [tag_to_pos(elem) for elem in predecessors]
    predecessors_percentages = ['%.2f' % (100.0 * c / sum(predecessors_counts))
                               for c in predecessors_counts]
    print 'Five most common tags before nouns'
    print table(['Tag'] + predecessors, ['Count'] + predecessors_counts,
                ['%'] + predecessors_percentages,
                ['Part of Speech'] + predecessors_pos)
    print ('Note that this result is very much in keeping with linguistic '
           'intuitions: one would expect articles, prepositions, adjectives, '
           'and other nouns to precede nouns.')

@print_function_name
def question_10():
    sent = ('Cyclophosphamide, vincristine, and prednisone plus rituximab'
            '(R-CVP)-treated patients gained a mean of 15.17 months in TWiST, '
            '8.33 months in Q-TwiST, and 11.30 months less in disease relapse, '
            'without increase in toxicity compared with cyclophosphamide, '
            'vincristine, and prednisone (CVP)-treated patients.')
    word_types = list(set(nltk.tokenize.word_tokenize(sent)))
    non_brown_words = [word for word in word_types if word not in BROWN_WORDS]
    print 'Consider the following sentence:\n"%s"' % sent
    print ('The words in this sentence that do not appear in the Brown Corpus '
           'are:\n\t%s' % '\n\t'.join(non_brown_words))
    print ('(Note that this sentence causes NLTK\'s word tokenizer to break '
           'because of poor named-entity recognition.)\n')
    words = {\
        '15.17':('Numbers can be captured with a regular expression. Thus, a '
                 'parser could try to parse an unkown token with the '
                 'number-regular-expression. If the parse is successful, we '
                 'know that the token is a cardinal (CD in the Brown Corpus '
                 'tagset). A similar approach can be used for determining if '
                 'the token is an ordinal (OD) (number regular expression '
                 'followed by one of "st", "nd", "rd", or "th") or a cardinal '
                 'genetive (CD$) (number regular expression followed by '
                 '"\'s").'),
        'R-CVP':('Most tokens placed directly before verbs and connected to '
                 'them with a hyphen will be nouns. Most pairs of hyphenated '
                 'tokens will have different parts of speech.'),
        'Q-TwiST':('If a word has more than one capital letter it probably is '
                   'an acronym i.e. a noun.'),
        'vincristine':('Words between nouns and determiners are most likely '
                       'nouns or adjectives. Additionally, words that occur as '
                       'part of a list (i.e. some number of singular tokens '
                       'separated by commas), it most likely has the same part-'
                       'of-speech as the other words (in this case: nouns).')}
    print 'Let us focus on the following words: %s' % ', '.join(words.keys())
    for (word, explanation) in words.items():
        print ('\tIn order to tag word %s, we could exploit the following '
               'properties. %s' % (word, explanation))
    print ('\nBasically: we can exploit morphological and grammatical features '
           'in order to infer part-of-speech information, thusly augmenting '
           'our stochastic tagging approach with a rules-based approach (in a '
           'way mimicking transformation based tagging).')
    print ('Note that for some parts-of-speech this is easier than others e.g. '
           'adjectives are very easy to spot in English due to their suffixes '
           '("-less", "-y", "-able", etc.) and their proximity to the noun '
           'they modify.')
    print ('Also note that for some languages this is easier than others e.g. '
           'nouns are capitalised in German, adjectives have very regular '
           'suffixes in French, etc.')
    print ('Additionally, we can also use linguistic knowledge to help infer '
           'part of speech information (e.g. unknown words are most likely to '
           'be proper nouns in English but common nouns or verbs in Mandarin '
           '(source: H. Tseng, D. Jurafsky, C. Manning. 2005. Morphological '
           'features help pos tagging of unknown words across language '
           'varieties. In Proceedings of the Fourth SIGHAN Workshop on Chinese '
           'Language Processing).')

@print_function_name        
def question_11():
    print ('Adding more domain-specific tag-types in order to parse sentences '
           'such as the ones found in Medline data is not a good idea.')
    print ('Strictly speaking, no additional tags are required: all the words '
           'in the Medline sample sentence that are not in the Brown Corpus '
           'can be precisely tagged with the existing tagset. Adding more tags '
           'will just lead to data-sparcity related problems when training '
           'taggers.')
    print ('The only thing we need is a mechanism to allow for part-of-speech '
           'entities to span multiple tokens (e.g. "prednisone plus '
           'rituximab") i.e. we want something like named entity recognition '
           'or multi word expression extraction. This will reduce the '
           'confusion in the tagger\'s transition model and in any grammar '
           'analysis one wants to perform to verify predicted tags (e.g. '
           'if a sequence of tags forces an uncommon parse tree, re-validate '
           'the tags with a different method).')

def answers():
    question_1()
    question_2()
    question_3()

    # Question 4
    print '#### Question 4 ####'
    # the tags of words in headline sentences are postfixed with "-HL"
    # we can thus eliminate all sentences from |BROWN_TAGGED_SENTS| that have 
    # only words with such tags in them
    is_headline = lambda sent: all([tag.endswith('-HL') for (_, tag) in sent])
    bns = [sent for sent in BROWN_TAGGED_SENTS if not is_headline(sent)]
    print ('Removing %d headline sentences from Brown Corpus'
            % (len(BROWN_TAGGED_SENTS) - len(bns)))
    print '\n\n'

    # Question 5
    print '#### Question 5 ####'
    print 'Generating a random 90/10 training/test split of the Brown Corpus'
    (train_set, test_set) = random_split(bns, 0.9)
    bigram_tagger = timeit(nltk.tag.BigramTagger, train_set,
        _pre_message='Training a bigram tagger', _postpend_time=True)
    bigram_tagger_accuracy = timeit(bigram_tagger.evaluate, test_set,
        _pre_message='Evaluating bigram tagger', _postpend_time=True)
    stdout_clearline()
    print 'Accuracy = %.2f%%' % (bigram_tagger_accuracy * 100)
    print '\n\n'

    # Question 6
    print '#### Question 6 ####'
    num_samples = 5
    print 'Tagging some training sentences with the bigram tagger'
    evaluate_tagger(bigram_tagger, train_set[:num_samples])
    print 'Tagging some test sentences with the bigram tagger'
    evaluate_tagger(bigram_tagger, test_set[:num_samples])
    print ('We observe that the bigram tagger works rather well on the '
           'training set: most sentences are tagged with the correct tags for '
           'each word. This good performance is expected as, by definition, '
           'the tagger has seen all of these pairs of word-tag combinations '
           'before. The results are not perfect - the tagger makes some '
           'mistakes with rare word/tag combinations - but these errors can be '
           'easily explained by the probabilistic nature of the parser (e.g. '
           'noun "right" predicted as adjective, a more common use of that '
           'word) and by its simplicity (e.g. the parser assumes that the tag '
           'of a word only depends on that word and the previous word\'s tag - '
           'a very simplifying/incorrect assumption). These errors are more '
           'common when tagging the test-set.')
    print ('Ways to fix this problem are to increase the amount of training '
           'data or the size of the tagger\'s context (i.e. use a higher-order '
           'N-gram model).')
    # is there anything ever that is _not_ improved by more training data...?
    print ('We also observe that the tagger breaks horribly on one of the test '
           'sentences: it predicts the correct tags for words up to a certain '
           'point, then does a misclassification, and thence onwards is unable '
           'to determine the tags for the remaining words in the sentence. '
           'This pattern is repeated when tagging sentences from the training '
           'set.')
    print ('The problem is caused by the fact that the bigram tagger follows a '
           'greedy strategy: it uses past predictions to influence '
           'future results - this means that incorrect predictions will have '
           'knock-on effects. Coupled with data sparcity and no smoothing for '
           'unseen words or tag sequences, this leads to aforementioned '
           'devastating results. One wrong prediction leads to a zero-count '
           'previous-tag/word combination. This means a zero probability for '
           'all tags for the current word - the word gets an not-possible-to-'
           'tag tag (symbolised in NLTK by the \'None\' tag) which then throws '
           'off the computations for the next tag... and so on.')
    print ('Sometimes these chains of not-possible-to-tag tag assignments are '
           'started without a word being misclassified. This is merely a '
           'special case of the description above: the current previous-tag/'
           'word pair has simply not being seen leading to the aforementioned '
           'chain of events (despite all tags up to that point being correct).')
    # didn't they teach us in machine learning class that using the outputs of
    # your classifier as inputs is a bad idea?
    print ('One possible way to fix the greediness problem is to use a simpler '
           '(e.g. unigram) tagger when the bigram tagger fails to generate a '
           'tag. NLTK implements this with the \'backoff\' argument of the '
           'nltk.tag.BigramTagger() constructor.')
    print ('Another potential solution to this problem is to increase the '
           'amount of context that the tagger refers to (i.e. use a N-gram '
           'tagger/model with N > 2). This should allow the tagger to '
           '\'recover\' from wrong tag predictions more easily as it would '
           'take more than one wrong prediction in a row for aforementioned '
           'greedy-effect to kick in. However, this approach will increase the '
           'amount of training data required to build a good tagger ('
           'introducing all the classic data-sparcity problems)... which makes '
           'the earlier proposed solution supperior.')
    print '\n\n'

    # Question 7
    print '#### Question 7 ####'
    hmm_tagger = timeit(nltk.tag.HiddenMarkovModelTagger.train, train_set,
        _pre_message='Training a HMM tagger', _postpend_time=True)
    hmm_tagger_accuracy = timeit(hmm_tagger.evaluate, test_set,
        _pre_message='Evaluating HMM tagger', _postpend_time=True)
    stdout_clearline()
    print 'Accuracy = %.2f%%' % (hmm_tagger_accuracy * 100)
    print 'Tagging the first test sentence with the HMM tagger'
    evaluate_tagger(hmm_tagger, test_set[:1])
    print ('The HMM tagger offers an almost four fold improvement over the '
           'bigram tagger.')
    print ('This can mainly be explained by the fact that the bigram tagger is '
           'an incredibly poor and over simplistic tagger, especially since no '
           'backoff model for unseen cases is used. A much larger training set '
           'would be required in order to get a decent performance.')
    print ('The results can also be explained by realising that the HMM tagger '
           'uses a more apt model of language than the very simplistic bigram '
           'tagger. For instance, the HMM\'s transition probabilities model '
           'how likely it is to change from a certain part-of-speech to '
           'another part-of-speech between words regardless of the words '
           'associated with the tags (e.g. articles preceed nouns). This '
           'dissociation of tag-from-token is less pronounced in the bigram '
           'model: the HMM tagger considers more context than the bigram '
           'tagger.')
    print ('The finding of the slightly more complex HMM models outperforming '
           'n-gram models is consistent with the literature (see: F.M. Hasan, '
           'N. UzZaman, and M. Khan. 2006. "Comparison of Different POS '
           'Tagging Techniques (n-grams, HMM and Brill\'s Tagger) for Bangla". '
           'SCS2 06 CIS2E 06. Also see the same authors\' paper in CLT07).')
    print '\n\n'

    # Question 8
    print '#### Question 8 ####'
    graph_info = {'100%':hmm_tagger_accuracy}
    graph_labels = ['100%']
    for i in range(3):
        slice_size = pow(2, i) / 6.0
        (subset, _) = random_split(train_set, slice_size)
        hmm_tagger = timeit(nltk.tag.HiddenMarkovModelTagger.train, subset,
            _pre_message=('Training a HMM tagger on %.2f%% of the training set'
            % (slice_size * 100)), _postpend_time=True)
        accuracy = timeit(hmm_tagger.evaluate, test_set,
            _pre_message='Evaluating HMM tagger', _postpend_time=True)
        stdout_clearline()
        print 'Accuracy = %.2f%%' % (accuracy * 100)
        tagger_name = '%.2f%%' % (slice_size * 100)
        graph_info[tagger_name] = accuracy
        graph_labels.append(tagger_name)
    bar_chart(graph_labels, [graph_info[key] for key in graph_labels],
              title='Effect of training-set size on HMM tagger accuracy',
              xlabel='Proportion of training-set trained on',
              ylabel='Accuracy (evaluated on same test set for each tagger)')
    print ('We see that the larger the training set, the higher the accuracy '
           'of the resulting HMM tagger (more training data = more seen words '
           '= less problematic/unseen words when tagging). However, after a '
           'certain point, increasing training set size leads to diminishing '
           'results in accuracy improvements (e.g. only 2% absolute accuracy '
           'improvement when adding 50% more training data going from 2/3 to '
           'full training set).')
    print '\n\n'

    # Question 9
    print '#### Question 9 ####'
    graph_info = {'Initial':hmm_tagger_accuracy}
    graph_labels = ['Initial']
    test_sets = [test_set]
    i = 0
    while i < 3:
        (train_set, test_set) = random_split(bns, 0.9)
        # make sure we haven't seen this split yet
        if test_set in test_sets:
            continue
        hmm_tagger = timeit(nltk.tag.HiddenMarkovModelTagger.train, train_set,
            _pre_message=('Training a HMM tagger on a new 90/10 split of the '
            'Brown Corpus'), _postpend_time=True)
        accuracy = timeit(hmm_tagger.evaluate, test_set,
            _pre_message='Evaluating HMM tagger', _postpend_time=True)
        stdout_clearline()
        print 'Accuracy = %.2f%%' % (accuracy * 100)
        test_sets.append(test_set)
        tagger_name = '#%d' % (i + 1)
        graph_info[tagger_name] = accuracy
        graph_labels.append(tagger_name)
        i += 1
    bar_chart(graph_labels, [graph_info[key] for key in graph_labels],
              title=('Effect of randomness on HMM tagger accuracy'),
              xlabel='Random 90/10 training/test split of the Brown Corpus',
              ylabel='Accuracy (evaluated unseen test sets for each tagger)')
    print ('We see that the way in which we split our corpus into training- '
           'and test-sets only has a negligible effect on the trained '
           'tagger\'s performance. Different splits will lead to marginally '
           'different accuracies, but as long as the splits are somewhat '
           'balanced (i.e. disregarding cases in which the training set does '
           'not contain some words/tags at all but the test set does), these '
           'fluctuations can be ignored. The HMM tagger\' performance does not '
           'stem from chance... but from large amounts of training data. One '
           'could even go as far as saying that it is not important what data '
           'we train on - only how much data we train on is relevant. (This '
           'statement is backed up by the fact that HMM training is largely '
           'unsupervised i.e. performance is bound to increase with training '
           'set size (leading to an increased language-coverage and therewith '
           'less unseen tokens implying higher accuracy)).')
    print '\n\n'

    question_10()
    question_11()

if __name__ == '__main__':
    def parse_error(message):
        sys.stderr.write('%s: %s\n' % (os.path.basename(__file__), message))
        exit(1)
    def parse_positional(argv, arg, arg_t):
        try:
            rval = argv.pop()
        except IndexError:
            parse_error(('option %s should be followed by a%s %s'
                          % (arg, 'n' if arg_t[0] in 'aeiou' else '', arg_t)))
        return rval
    random.seed(__RANDOM_SEED__)
    argv = list(reversed(sys.argv[1:]))
    while len(argv) > 0:
        arg = argv.pop()
        if arg == '--no-display':
            __NO_DISPLAY__ = True
        elif arg == '--random-seed':
            random.seed(parse_positional(argv, arg, 'int'))
        elif arg == '--add-to-pythonpath':
            paths = parse_positional(argv, arg, 'string')
            paths = list(reversed(paths.split(os.pathsep)))
            [sys.path.insert(0, path) for path in paths]
        else:
            parse_error('unrecognized option %s' % arg)
    answers()
